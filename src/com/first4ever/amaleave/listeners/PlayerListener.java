package com.first4ever.amaleave.listeners;

import java.text.DecimalFormat;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.first4ever.amaleave.Core;
import com.first4ever.amaleave.Managers.Config;

public class PlayerListener implements Listener
{

	// ---------- Properties -------------

	private Core _plugin;
	private Config _conf;



	// ---------- Temp vars -------------

	private ItemStack _empty;	



	// ---------- Constructor -------------

	public PlayerListener(Core plugin)
	{
		_plugin = plugin;
		_conf = plugin._conf;
		_empty = new ItemStack(0);
	}



	// ---------- Listeners -------------

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamage(EntityDamageByEntityEvent e)
	{
		if (e.isCancelled())
			return;

		Entity att = e.getDamager();
		Entity def = e.getEntity();

		if (! (def instanceof Player))
			return;

		if (att instanceof Projectile)
			att = ((Projectile)att).getShooter();

		if (! (att instanceof Player))
			return;

		Player p = (Player)def;

		_conf.setLastAttacker(((Player)att).getName(), p.getName());
		_conf.setLastAttackTime(p.getName());
	}



	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDeath(EntityDeathEvent e)
	{
		if (e.getEntity() instanceof Player)
		{
			Player p = (Player)e.getEntity();
			_conf.resetLastAttackTime(p.getName());
			_conf.resetLastAttacker(p.getName());
		}
	}



	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		Player p = e.getPlayer();

		long time = _conf.getTime() - _conf.getLastAttackTime(p.getName());

		if (time > Config._resetTime || p.hasPermission("ama.leave.disconnect"))
		{
			_conf.resetLastAttackTime(p.getName());
			return;
		}

		_conf.setUnbanTime(p.getName());

		String text = formatLeaveMessage(p.getName(), _conf.getLastAttacker(p.getName()));
		_plugin._log.log(text);

		if (Config._killOnLeave)
		{
			p.setHealth(0);

			PlayerInventory inv = p.getInventory();
			Location loc = p.getLocation();

			for (ItemStack it : inv.getContents())
			{
				if (it != null && it.getTypeId() != 0)
					loc.getWorld().dropItem(loc, it);
			}

			for (ItemStack it : inv.getArmorContents())
			{
				if (it != null && it.getTypeId() != 0) 
					loc.getWorld().dropItem(loc, it);
			}

			p.getInventory().clear();
			inv.setBoots(_empty);
			inv.setHelmet(_empty);
			inv.setChestplate(_empty);
			inv.setLeggings(_empty);
		}

		if (Config._broadcastOnLeave)
			broadcastLeave(text);
		
		p.saveData();
	}



	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerLoginEvent e)
	{
		Player p = e.getPlayer();
		long time = _conf.getTime() - _conf.getUnbanTime(p.getName());

		if (time > 0)
		{
			_conf.resetUnbanTime(p.getName());
			_conf.resetLastAttackTime(p.getName());
			_conf.resetLastAttacker(p.getName());
		}
		else if (! p.hasPermission("ama.leave.nodelay"))
			e.disallow(Result.KICK_OTHER, (Config._reconnectMessage.replace("%time", formatTime(-time))));
	}



	// ========== UTIL ====================================

	private void broadcastLeave(String msg)
	{
		String text = ChatColor.GRAY + msg;

		for (Player p : _plugin.getServer().getOnlinePlayers())
			p.sendMessage(text);
	}

	private String formatLeaveMessage(String leaver, String attacker)
	{
		return Config._leaveMessage.replace("%leaver", leaver).replace("%attacker", attacker);
	}

	private String formatTime(long millis)
	{
		millis = millis / 1000;
		int _seconds = (int) (millis % 60);
		int _minutes = (int) ((millis / 60) % 60);
		int _hours   = (int) ((millis / 3600) % 24);

		return (formatZeros(_hours) + "h" + formatZeros(_minutes) + "m" + formatZeros(_seconds) + "s");
	}

	private String formatZeros(int i)
	{
		return (new DecimalFormat("00")).format(i);
	}

}