package com.first4ever.amaleave.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.first4ever.amaleave.Core;
import com.first4ever.amaleave.Managers.Config;

public class CommandListener implements CommandExecutor
{

	// ---------- Properties -------------

	private Config _conf;



	// ---------- Temp vars -------------

	private Player _s;	
	private List<String> _help;



	// ---------- Constructor -------------

	public CommandListener(Core plugin)
	{
		_conf = plugin._conf;
		_help = createHelp();
	}



	// ---------- Command Managing -------------

	public boolean onCommand(CommandSender sender, Command command, String alias, String[] a)
	{
		if (! command.getName().equalsIgnoreCase("amaleave"))
			return true;

		if (! (sender instanceof Player))
		{
			sendIngameCommandOnly(sender);
			return true;
		}

		_s = (Player)sender;

		// ARGS NUMBER
		if (a.length != 1)
		{
			sendInvalidArgsNumber();
		}
		// PERMISSION
		else if (! _s.hasPermission("ama.leave.admin"))
		{
			sendHaveNotPermission();
		}
		// HELP
		else if (a[0].equals("?") || a[0].equalsIgnoreCase("help"))
		{
			sendHelp();
		}
		// SAVE
		else if (a[0].equalsIgnoreCase("save"))
		{
			commandSave();
		}
		// RELOAD
		else if (a[0].equalsIgnoreCase("reload"))
		{
			commandReload();
		}
		// ERROR
		else
		{
			sendCommandError();
		}

		return true;
	}



	// ========== COMMANDS ====================================

	private void commandSave()
	{
		_conf.save();
		good("Configuration sauvegard�e.");
	}

	private void commandReload()
	{
		_conf.reload();
		good("Configuration recharg�e.");
	}



	// ========== SEND ====================================

	private List<String> createHelp()
	{
		List<String> list = new ArrayList<String>(6);
		
		list.add("==================== AmaLeave =====================");
		list.add("");
		list.add(ChatColor.GOLD + "/amaleave [?/help]" + ChatColor.BLUE + " : Afficher l'aide.");
		list.add(ChatColor.GOLD + "/amaleave save" + ChatColor.BLUE + " : Sauvegarder le fichier.");
		list.add(ChatColor.GOLD + "/amaleave reload" + ChatColor.BLUE + " : Recharger le fichier.");
		list.add("");
		
		return list;
	}
	
	private void sendHelp()
	{
		_s.sendMessage(_help.toArray(new String[_help.size()]));
	}

	private void sendInvalidArgsNumber()
	{
		error("Nombre d'arguments incorrect.");
	}

	private void sendIngameCommandOnly(CommandSender cs)
	{
		cs.sendMessage("Les commandes doivent �tre ex�cut�es InGame.");
	}

	private void sendHaveNotPermission()
	{
		error("Vous n'avez pas la permission d'utiliser cette commande !");
	}

	private void sendCommandError()
	{
		error("Commande incorrecte. '/amaleave ?' pour afficher la liste des commandes disponibles.");
	}



	// ========== UTIL ====================================

	private void good(String s)
	{
		_s.sendMessage(ChatColor.GREEN + s);
	}

	private void error(String s)
	{
		_s.sendMessage(ChatColor.RED + s);
	}

}