package com.first4ever.amaleave;

import org.bukkit.plugin.java.JavaPlugin;

import com.first4ever.amaleave.Managers.Config;
import com.first4ever.amaleave.Managers.Logging;
import com.first4ever.amaleave.listeners.CommandListener;
import com.first4ever.amaleave.listeners.PlayerListener;

public class Core extends JavaPlugin
{

	public Config _conf;
	public Logging _log;



	@Override
	public void onDisable()
	{
		_conf.save();
	}

	@Override
	public void onEnable()
	{
		_log = new Logging();
		_conf = new Config(this);
		_conf.load();

		this.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		getCommand("amaleave").setExecutor(new CommandListener(this));

		this.getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable()
		{
			public void run()
			{
				_conf.save();
			}	
		}, Config._delayBetweenSaves * 20L, Config._delayBetweenSaves * 20L);
	}
	
}