package com.first4ever.amaleave.Managers;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Logging
{
	
	// ---------- Properties -------------
	
	private Logger _logger;
	private final String _prefix = "[AmaLeave] ";
	
	
	
	// ---------- Constructor -------------
	
	public Logging()
	{
		_logger = Logger.getLogger("Minecraft");
	}

	
	
	// ---------- Methods -------------
	
	public void log(String s)
	{
		log(s, Level.INFO);
	}
	
	public void log(String s, Level l)
	{
		_logger.log(l, _prefix + s);
	}
	
}