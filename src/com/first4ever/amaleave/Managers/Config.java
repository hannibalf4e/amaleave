package com.first4ever.amaleave.Managers;

import org.bukkit.configuration.file.FileConfiguration;

import com.first4ever.amaleave.Core;

public class Config
{

	// ---------- Properties -------------

	private Core _plugin;
	private FileConfiguration _config;

	public static long _banTime;
	public static long _resetTime;
	
	public static boolean _killOnLeave;
	public static boolean _broadcastOnLeave;
	
	public static String _leaveMessage;
	public static String _reconnectMessage;
	
	public static int _delayBetweenSaves;


	
	// ---------- Constructor -------------

	public Config(Core plugin)
	{
		_plugin = plugin;
	}

	
	
	// ---------- Load & Save -------------

	public void load()
	{
		_config = _plugin.getConfig();
		loadOptions();
		_plugin.saveConfig();
		_plugin._log.log("Config loaded");
	}

	public void reload()
	{
		_plugin.reloadConfig();
		load();
	}

	public void save()
	{
		_plugin.saveConfig();
		_config = _plugin.getConfig();
		_plugin._log.log("Config saved");
	}
	
	
	
	// ---------- Methods -------------

	public void setLastAttacker(String att, String def)
	{
		_config.set("players." + def + ".lastAttacker", att);
	}
	
	public void setLastAttackTime(String def)
	{
		_config.set("players." + def + ".lastAttackTime", getTime());
	}
	
	public void setUnbanTime(String leaver)
	{
		_config.set("players." + leaver + ".unbanTime", getPreviewedUnbanTime());
	}
	
	
	
	public void resetLastAttackTime(String def)
	{
		_config.set("players." + def + ".lastAttackTime", 0L);
	}
	
	public void resetUnbanTime(String leaver)
	{
		_config.set("players." + leaver + ".unbanTime", 0L);
	}
	
	public void resetLastAttacker(String def)
	{
		_config.set("players." + def + ".lastAttacker", "");
	}
	
	
	
	public long getUnbanTime(String leaver)
	{
		return _config.getLong("players." + leaver + ".unbanTime", 0L);
	}
	
	public String getLastAttacker(String def)
	{
		return _config.getString("players." + def + ".lastAttacker");
	}
	
	public long getLastAttackTime(String def)
	{
		return _config.getLong("players." + def + ".lastAttackTime", 0L);
	}
	

	
	// ---------- Options -------------

	private void loadOptions()
	{
		_config.addDefault("options.banTime", 120);
		_config.addDefault("options.resetTime", 30);
		_config.addDefault("options.killOnLeave", true);
		_config.addDefault("options.broadcastOnLeave", true);
		_config.addDefault("options.leaveMessage", "%leaver a fui devant %attacker !");
		_config.addDefault("options.reconnectMessage", "Impossible de se reconnecter : %time restantes !");
		_config.addDefault("options.delayBetweenSaves", 600);
		_config.options().copyDefaults(true);
		
		_banTime = _config.getInt("options.banTime") * 1000;
		_resetTime = _config.getInt("options.resetTime") * 1000;
		_killOnLeave = _config.getBoolean("options.killOnLeave");
		_broadcastOnLeave = _config.getBoolean("options.broadcastOnLeave");
		_leaveMessage = _config.getString("options.leaveMessage");
		_reconnectMessage = _config.getString("options.reconnectMessage");
		_delayBetweenSaves = _config.getInt("options.delayBetweenSaves");
	}

	
	
	// ---------- Utils -------------
	
	public long getTime()
	{
		return System.currentTimeMillis();
	}
	
	private long getPreviewedUnbanTime()
	{
		return getTime() + _banTime;
	}

}